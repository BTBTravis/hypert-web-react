const LOCAL_STORAGE_KEY = 'hypert-web';

export default class Safe {
    save(apiKey, host) {
        localStorage.setItem(LOCAL_STORAGE_KEY, apiKey + '||' + host);
        return {
            apiKey: this.apiKey,
            host: this.host
        }
    }

    get host() {
        let parts = this.load();
        let key = parts ? parts[1] : false;
        return key;
    }

    get apiKey() {
        let parts = this.load();
        let key = parts ? parts[0] : false;
        return key;
    }

    load() {
        let payload = localStorage.getItem(LOCAL_STORAGE_KEY);
        console.log('loading payload: ', payload);
        return payload ? payload.split('||') : false;
    }
}
