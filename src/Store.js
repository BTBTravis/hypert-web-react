import { decorate, observable, computed, action } from 'mobx'
import Safe from './Safe'
import Hypert from './Hypert'

export const EDIT = 'edit';
export const CREATE = 'create';
export const BACKLOG = 'backlog';
export const TODAY = 'today';

const safe = new Safe();
const hypert = new Hypert({
  apiKey: safe.apiKey,
  host: safe.host
});

class preBaseStore {
  // API_KEY and HOST Stuff
  apiKey = '';
  host = '';
  loadFromSafe() {
    this.apiKey = safe.apiKey || '';
    this.host = safe.host || '';
    hypert.config = {
      apiKey: this.apiKey,
      host: this.host
    }
    this.refreshLinks();
  }

  get isInstallFormOpen() {
    return this.apiKey.length === 0 && this.host.length === 0;
  }

  install(apiKey, host) {
    safe.save(apiKey, host);
    this.loadFromSafe();
  }


  formEditingMode = CREATE;

  get formTitle() {
    return this.formEditingMode === CREATE ? "NEW" : "UPDATE";
  }

  formOpen = false;
  formAvalable = false;
  toggleForm() {
    if (!this.formOpen && !this.formAvalable) {
      this.formAvalable = true;
      setTimeout(() => this.formOpen = true, 10);
    } else {
      this.formAvalable = false;
      this.formOpen = false;
    }
  }

  saving = false;
  error = false;
  errorMessage = '';
  createLink(title, url, brk) {
    this.saving = true;
    return hypert.create(...arguments)
          .then(data => {
            console.log('data: ', data);
            if (data.hasOwnProperty('errors')) {
              this.saving = false;
              this.errorMessage = JSON.stringify(data.errors);
              this.error = true;
              return false;
            } else {
              this.saving = false;
              this.error = false;
            }
            return true;
          })
          // .catch(e => {
          //   console.log('e: ', e);
          //   return false;
          // });
  }

  linkListMode = TODAY;

  setLinkListMode(mode) {
    this.linkListMode = mode;
    this.refreshLinks();
  }

  links = [];

  refreshLinks() {
    if (this.linkListMode === TODAY ) {
      hypert.today().then(res => {
        console.log('res: ', res.data);
        this.links = res.data;
      });
    } else {
      hypert.backlog().then(res => {
        console.log('res: ', res.data);
        this.links = res.data;
      });
    }
  }

  viewLink(index) {
    hypert.view(this.links[index]).then(ok => {
      if (ok) {
        window.open(this.links[index].url, '_blank');
        this.refreshLinks();
      }
    });
  }

  fakeViewLink(id) {
    hypert.view(this.links[id]).then(ok => {
      if (ok) {
        this.refreshLinks();
      }
    });
  }

  deleteLink(id) {
    hypert.delete(this.links[id]).then(res => {
      this.refreshLinks();
    });
  }
}


export const BaseStore = decorate(preBaseStore, {
  host: observable,
  apiKey: observable,
  loadFromSafe: action.bound,
  isInstallFormOpen: computed,
  install: action.bound,

  formEditingMode: observable,
  formTitle: computed,
  formOpen: observable,
  formAvalable: observable,
  toggleForm: action.bound,
  saving: observable,
  error: observable,

  linkListMode: observable,
  setLinkListMode: action.bound,
  links: observable,
  refreshLinks: action.bound,
  viewLink: action.bound,
  fakeViewLink: action.bound,
  deleteLink: action.bound,
});

// tick: action
