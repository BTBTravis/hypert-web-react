export default class Hypert {
    constructor(config) {
        this.apiKey = config.apiKey;
        this.host = config.host;
    }

    apiKey = '';
    host = '';

    set config(opt) {
        this.apiKey = opt.apiKey;
        this.host = opt.host;
    }

    post(endpoint, data = {}) {
        const url = this.host + '/api' + endpoint;
        return fetch(url, {
            method: 'POST', // *GET, POST, PUT, DELETE, etc.
            mode: 'cors', // no-cors, cors, *same-origin
            cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
            // credentials: 'include', // include, *same-origin, omit
            headers: new Headers({
                'Authorization': 'key ' + this.apiKey,
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            }),
            // redirect: "follow", // manual, *follow, error
            // referrer: "no-referrer", // no-referrer, *client
            body: JSON.stringify(data), // body data type must match "Content-Type" header
        })
            .then(response => response.json()); // parses response to JSON
    }

    httpGet(endpoint, data = {}) {
        const url = this.host + '/api' + endpoint;
        return fetch(url, {
            method: 'GET', // *GET, POST, PUT, DELETE, etc.
            mode: 'cors', // no-cors, cors, *same-origin
            headers: new Headers({
                'Authorization': 'key ' + this.apiKey,
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            }),
        })
    }

    create(title, url, brk) {
        return this.post('/links', {
            link: {
                url: url,
                title: title,
                break: brk
            }
        })
    }

    backlog() {
        return this.httpGet('/links/backlog')
                   .then(res => res.json());
    }

    today() {
        return this.httpGet('/links/todays')
                   .then(res => res.json());
    }

    view(link) {
        return this.httpGet('/links/view/' + link.id)
                   .then(res => res.ok) // JSON-string from `response.json()` call
    }

    delete(link) {
        return this.httpGet('/links/delete/' + link.id)
    }
}
