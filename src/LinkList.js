import React, { Component } from 'react';
import { observer } from 'mobx-react';
import { BACKLOG, TODAY } from './Store';

const LinkList = observer(class extends Component {
    render() {
        const { linkListMode, links } = this.props.basicStore;

        console.log('links: ', links);
        return (
            <div className="links__inner-wrapper">
                <div className="link-tabs">
                    <div onClick={e => this.props.basicStore.setLinkListMode(TODAY)} className={ linkListMode === TODAY ? 'link-tabs__tab is-active' : 'link-tabs__tab' }>
                        <p className='link-tabs__title'>Today</p>
                    </div>
                    <div onClick={e => this.props.basicStore.setLinkListMode(BACKLOG)} className={ linkListMode === BACKLOG ? 'link-tabs__tab is-active' : 'link-tabs__tab' }>
                        <p className='link-tabs__title'>Backlog</p>
                    </div>
                </div>
                <div className="links">
                    {Object.keys(links).map((linkKey) => {
                        const link = links[linkKey];
                        link.key =  linkKey;
                        return <Link key={linkKey} link={link} store={this.props.basicStore}/>;
                    })}
                </div>
            </div>
        );
    }
});


const Link = observer(class extends Component {
    handleTrashClick = () => {
        this.props.store.deleteLink(this.props.link.key);
    }

    handleEyeClick = () => {
        this.props.store.fakeViewLink(this.props.link.key);
    }

    handleEditClick = () => {
        console.log('--->handleEditClick');
    }

    handleLinkClick = () => {
        this.props.store.viewLink(this.props.link.key);
    }

    render() {
        const { link } = this.props;
        return (
            <div className='link'>
                <div onClick={this.handleLinkClick} className='link__title-bar'>
                    <div className='link__break-icon'><p>{link.break}</p></div>
                    <p className='link__title'>{link.title}</p>
                </div>
                <div className='link__body'>
                    <p onClick={this.handleLinkClick} className='link__url'>{link.url}</p>
                    <div className='link__icons'>
                        <i onClick={this.handleTrashClick} className='fas fa-trash'></i>
                        <span style={{opacity: 0.5, lineHeight: 0}}><i onClick={this.handleEditClick} className="fas fa-edit"></i></span>
                        <i onClick={this.handleEyeClick} className="fas fa-eye"></i>
                    </div>
                </div>
            </div>
        );
    }
});


// <div onClick={this.handleLinkTabClick('today')} className={ linkListMode === 'today' ? 'link-tabs__tab is-active' : 'link-tabs__tab' }><p>Today</p></div>
// <div onClick={this.props.basicStore.setLinkListMode('backlog')} className={ linkListMode === 'backlog' ? 'link-tabs__tab is-active' : 'link-tabs__tab' }><p>Backlog</p></div>
export default LinkList;
