import React, { Component } from 'react';
import './App.css';
import { observer } from 'mobx-react';
import LinkList from './LinkList';

const ApiOverlay = observer(class ApiOverlay extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      host: '',
      apiKey: '',
    };
  }

  handleSubmit(evt) {
    evt.preventDefault();
    console.log('---->API OVERLAY SUBMIT');
    const { host, apiKey } = this.state;
    if (host !== '' && apiKey !== '') {
      this.props.basicStore.install(apiKey, host);
    }
  }
  render() {
    const { isInstallFormOpen } = this.props.basicStore;
    return (
      <div className={isInstallFormOpen ? "api-overlay__wrapper is-open" : "api-overlay__wrapper"} >
        <form onSubmit={this.handleSubmit.bind(this)} className="api-overlay__body">
          <p className="hypert-form__title">INSTALL</p>
          <input
            className="hypert-form__input"
            placeholder="HOST"
            value={this.state.host}
            onChange={evt => this.setState({ host: evt.target.value })}
            type="text"
          />
          <input
            className="hypert-form__input"
            placeholder="API_KEY"
            value={this.state.apiKey}
            onChange={evt => this.setState({ apiKey: evt.target.value })}
            type="text"
          />
          <input className="hypert-form__btn" type="submit" value="SAVE"/>
        </form>
      </div>
    );
  }
});

const App = observer(class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: '',
      url: '',
      break: '',
    };
  }
  onAddBtnClick(evt) {
    evt.preventDefault();
    console.log('---->onAddBtnClick');
    console.log('formOpen: ', this.props.basicStore.formOpen);
    this.props.basicStore.toggleForm();
  }
  formClasses() {
    let mainClass = "link-form__wrapper";
    if (this.props.basicStore.formOpen) mainClass += " is-open";
    if (this.props.basicStore.formAvalable) mainClass += " is-avalable";
    return mainClass;
  }
  handleSubmit(evt) {
    evt.preventDefault();
    console.log('---->LINK FORM OVERLAY SUBMIT');
    const { title, url, brk } = this.state;
    if (title !== '' && url !== '' && brk !== '') {
      this.props.basicStore.createLink(title, url, brk)
          .then(success => {
            if (success) {
              this.setState({ title: '' });
              this.setState({ url: '' });
              this.setState({ brk: '' });
            }
          });
    }
  }

  render() {

    console.log('store: ', this.props.basicStore);
    return (
      <div className="app-wrapper">
        <div className="links__wrapper">
          <LinkList basicStore={this.props.basicStore} />
        </div>

        <div className="circle-wrapper">
          <div className={this.props.basicStore.formOpen ? "circle is-active" : "circle"}> </div>
          <div className="add-icon" onClick={this.onAddBtnClick.bind(this)} >
            <svg  width="36" height="36" viewBox="0 0 36 36" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path className={this.props.basicStore.formOpen ? "add-icon__vert_bar is-open" : "add-icon__vert_bar"} d="M18 33V17L18 3" stroke="white" strokeWidth="5" strokeLinecap="round" strokeLinejoin="round"/>
              <path d="M3 18L19 18L33 18" stroke="white" strokeWidth="5" strokeLinecap="round" strokeLinejoin="round"/>
            </svg>
          </div>
        </div>
        <div className={this.formClasses()}>
          <form onSubmit={this.handleSubmit.bind(this)} className={this.props.basicStore.saving ? "link-form is-saving" : "link-form"}>
            <p className="hypert-form__title">
              {this.props.basicStore.formTitle}
            </p>
            { this.props.basicStore.error ?
              <p className="hypert-form__error">Error:{this.props.basicStore.errorMessage}</p>
              :
              ''
            }
            <input
              className="hypert-form__input"
              placeholder="TITLE"
              value={this.state.title}
              onChange={evt => this.setState({ title: evt.target.value })}
              type="text"
            />
            <input
              className="hypert-form__input"
              placeholder="URL"
              value={this.state.url}
              onChange={evt => this.setState({ url: evt.target.value })}
              type="text"
            />
            <input
              className="hypert-form__input"
              placeholder="BREAK"
              value={this.state.brk}
              onChange={evt => this.setState({ brk: evt.target.value })}
              type="text"
            />
            <input className="hypert-form__btn" type="submit" value="SAVE"/>
          </form>
        </div>
        <ApiOverlay basicStore={ this.props.basicStore }/>
      </div>
    );
  }
})

export default App;
